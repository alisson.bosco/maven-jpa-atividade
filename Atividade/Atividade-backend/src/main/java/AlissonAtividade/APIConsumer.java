package AlissonAtividade;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

public class APIConsumer {

    public static void main( String[] args ) {
        OkHttpClient client = new OkHttpClient();
        String apiUrl = "https://rickandmortyapi.com/api/character/";

        try {
            Request request = new Request.Builder().url( apiUrl ).build();
            Response response = client.newCall( request ).execute();
            if ( response.isSuccessful() ) {
                String responseBody = response.body().string();
                JSONArray characterArray = new JSONObject( responseBody ).getJSONArray( "results" );
                for ( int i = 0; i < characterArray.length(); i++ ) {
                    String name = characterArray.getJSONObject( i ).getString( "name" );
                    System.out.println( name );
                }
            } else {
                System.out.println( "Erro ao obter a lista de personagens " + response.code() );
            }
        } catch ( IOException e ) {
            System.out.println( "Houve um erro ao fazer a chamada à API: " + e.getMessage() );
        }
    }
}
