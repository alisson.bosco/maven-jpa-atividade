package AlissonAtividade;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Condominio {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    private String nome;

    @OneToMany( cascade = CascadeType.ALL, orphanRemoval = true )
    private List< Imovel > imoveis = new ArrayList<>();

    public List< Imovel > getImoveis() {
        return imoveis;
    }

    public void setImoveis( List< Imovel > imoveis ) {
        this.imoveis = imoveis;
    }

    public Long getId() {
        return id;
    }

    public void setId( Long id ) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome( String nome ) {
        this.nome = nome;
    }
    // Getters, setters e construtores
}