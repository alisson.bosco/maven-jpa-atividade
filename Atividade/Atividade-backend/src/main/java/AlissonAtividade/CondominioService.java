package AlissonAtividade;

import java.io.IOException;
import java.util.List;
import javax.persistence.EntityManager;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CondominioService {

    private EntityManager entityManager;
    private ObjectMapper objectMapper = new ObjectMapper();

    public CondominioService( EntityManager entityManager ) {
        this.entityManager = entityManager;
    }

    public String toJson( List< Condominio > condominios ) throws JsonProcessingException {
        return objectMapper.writeValueAsString( condominios );
    }

    public void saveFromJson( String json ) throws JsonProcessingException {
        Condominio[] condominios = objectMapper.readValue( json, Condominio[].class );
        for ( Condominio condominio : condominios ) {
            entityManager.persist( condominio );
        }
    }

    public String toJsonCondominiosEImoveis() throws JsonProcessingException {
        List< Condominio > condominios = entityManager.createQuery( "SELECT c FROM Condominio c", Condominio.class )
                .getResultList();
        return objectMapper.writeValueAsString( condominios );
    }

    public void saveCondominiosEImoveisFromJson( String json ) throws JsonProcessingException, IOException {
        Condominio[] condominios = objectMapper.readValue( json, Condominio[].class );
        for ( Condominio condominio : condominios ) {
            entityManager.persist( condominio );
        }
    }

    public String toJsonCondominiosComMaisDe3Imoveis() throws JsonProcessingException {
        List< Condominio > condominiosComMaisDe3Imoveis = entityManager
                .createQuery( "SELECT c FROM Condominio c WHERE SIZE(c.imoveis) > 3", Condominio.class )
                .getResultList();
        return objectMapper.writeValueAsString( condominiosComMaisDe3Imoveis );
    }

    public String toJsonCondominiosComAlugueisAbaixoDeX( double valorMaximoAluguel ) throws JsonProcessingException {
        List< Condominio > condominiosComAlugueisAbaixoDeX = entityManager
                .createQuery( "SELECT c FROM Condominio c WHERE EVERY(i.valorAluguel < :valorMaximoAluguel)",
                        Condominio.class )
                .setParameter( "valorMaximoAluguel", valorMaximoAluguel ).getResultList();
        return objectMapper.writeValueAsString( condominiosComAlugueisAbaixoDeX );
    }
}
