package AlissonAtividade;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {

    public static void main( String[] args ) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory( "AlissonDb" );
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        CondominioService condominioService = new CondominioService(entityManager);

        try {
            Imovel imovel1 = new Imovel();
            imovel1.setDescricao( "Casa grande com jardim" );
            imovel1.setValorAluguel( 2500.0 );

            Imovel imovel2 = new Imovel();
            imovel2.setDescricao( "Apartamento com vista para o mar" );
            imovel2.setValorAluguel( 1800.0 );

            Condominio condominio1 = new Condominio();
            condominio1.setNome( "Condominio A" );
            condominio1.getImoveis().add( imovel1 );

            Condominio condominio2 = new Condominio();
            condominio2.setNome( "Condominio B" );
            condominio2.getImoveis().add( imovel1 );
            condominio2.getImoveis().add( imovel2 );

            String json = condominioService.toJsonCondominiosEImoveis();
            condominioService.saveCondominiosEImoveisFromJson( json );

            String jsonCondominiosEImoveis = condominioService.toJsonCondominiosEImoveis();
            System.out.println( "JSON dos condomínios e seus imóveis:" );
            System.out.println( jsonCondominiosEImoveis );

            String jsonCondominiosComMaisDe3Imoveis = condominioService.toJsonCondominiosComMaisDe3Imoveis();
            System.out.println( "\nJSON dos condomínios com mais de 3 imóveis:" );
            System.out.println( jsonCondominiosComMaisDe3Imoveis );

            double valorMaximoAluguel = 2000.0;
            String jsonCondominiosComAlugueisAbaixoDeX = condominioService
                    .toJsonCondominiosComAlugueisAbaixoDeX( valorMaximoAluguel );
            System.out.println( "\nJSON dos condomínios com alugueis abaixo de " + valorMaximoAluguel + ":" );
            System.out.println( jsonCondominiosComAlugueisAbaixoDeX );
        } catch ( Exception e ) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            entityManagerFactory.close();
        }
    }
}
